# Agora Vote iOS

## Intern Name: Aleksandra Komagorkina ([@akomagorkina](https://gitlab.com/akomagorkina))
 
## Organization: [AOSSIE](https://aossie.gitlab.io/)

## [Agora iOS](https://gitlab.com/aossie/agora-ios/)

## [Source Code](https://gitlab.com/aossie/agora-ios/-/tree/gsoc-2022)

## Mentor: [Abhishek Agarwal](https://gitlab.com/ABHI165)

### Overview of the Application

Agora vote iOS allows users to create elections, invite voters, view results etc. This application uses Agora Web API as a backend application. It allows for elections to be held by using multiple algorithms such as Oklahoma, RangeVoting. The iOS project has started in 2020 and some intial UI was implemented back then, but there was no functional API support or any business logic integration up until now.

### Goals

My mentor and I changed the project scope and goals at the first weeks of coding period. Together we decided to focus my project on exploring and implementing The Composable Architecture (TCA) as well as continuing the implementation of SwiftUI while following the User Interface Guidelines and iOS best practices. 

The introduction of TCA was justified by the need to better bind together views in SwiftUI and potential business logic represented as state machines for additional robustness, so the Composable Architecture fit in it nicely. 

My goal was to build an authentication and authorisation layers, as well as to connect together Create Election flows and remote API, as well as to update and clean up the UI. 

Here are the steps which were taken to reach those goals:

First part of the coding period was focused on the Authorization flow: 

- [Migrate from Cathage to Swift Package Manager](https://gitlab.com/aossie/agora-ios/-/merge_requests/49) - **Done**
- [Introduce The Composable Architecture to the project](https://gitlab.com/aossie/agora-ios/-/merge_requests/50) - **Done**
- [Create finite state machine for the Sign Up flow](https://gitlab.com/aossie/agora-ios/-/merge_requests/51) - **Done**
- [Introduce Session loader to lay out the foundation for the session management](https://gitlab.com/aossie/agora-ios/-/merge_requests/51) - **Done**
- [Update User model to support tokens and to align with the API](https://gitlab.com/aossie/agora-ios/-/merge_requests/52) - **Done**
- [Implement Sign-in flow using TCA](https://gitlab.com/aossie/agora-ios/-/merge_requests/52) - **Done**
- [Add a TCA-driven Navigation layer](https://gitlab.com/aossie/agora-ios/-/merge_requests/54) - **Done**
- [Add a view for password reset](https://gitlab.com/aossie/agora-ios/-/merge_requests/54) - **Done**
- [Add Sign-up view](https://gitlab.com/aossie/agora-ios/-/merge_requests/54) - **Done**
- [Add a view for email sent confirmation](https://gitlab.com/aossie/agora-ios/-/merge_requests/54) - **Done**
- [Complete the authorization flow by pluging in the Authorization UI into TCA and connecting to the network layer](https://gitlab.com/aossie/agora-ios/-/merge_requests/54) - **Done**

Second part of the coding period was focused on the Create election flow and polishing the code: 

- [Align create election model with API](https://gitlab.com/aossie/agora-ios/-/merge_requests/55) - **Done**
- [Create finite state machine for the Create election flow](https://gitlab.com/aossie/agora-ios/-/merge_requests/56) - **Done**
- [Align Create election UI with the updated model](https://gitlab.com/aossie/agora-ios/-/merge_requests/56) - **Done**
- [Bind Create Election controls to TCA](https://gitlab.com/aossie/agora-ios/-/merge_requests/56) - **Done**
- [Add a message view to the Create Election](https://gitlab.com/aossie/agora-ios/-/merge_requests/58) - **Done**
- [Connect Create Election flow to the Network layer](https://gitlab.com/aossie/agora-ios/-/merge_requests/58) - **Done**

I dedicated some time in the Final Evaluation week to add missing documentation and address mentor's feedback as well as to complete final work submission.

Enhancement features:
- [Add support for a Mocked Server](https://gitlab.com/aossie/agora-ios/-/merge_requests/53) - **Done**  - Sometimes the server fails to send a confirmation email. Introducing a way to Mock a server response would both unblock the developers and allow for future support in Unit tests.

### Summary of whole GSoC journey
​
I didn't have any prior experience of contributing to AOSSIE or any other open source project so it was a completely new experience for me. 

I have never worked with TCA before and was really happy to dedicate some time to dive in into learning and exploring the topic. Introducing a clear definition of State, Store, Action, Environment, and Reducers to the app was a wonderful idea since it allows to support better State management in SwiftUI. 

I have started the first part of the coding period by updating project settings and frameworks. The next step was the initial introduction of the TCA to the app: together with the mentor we've decided to start implementing TCA in the authorization flow first.

The app was missing the networking layer as well as a few screens. The introduction of the TCA also required updating the navigation layer of the app, so I have not only added the missing UI and aligned it with the API but also added a separate Navigation and Networking layers, as well as created a Test store for the authentication flow. My mentor suggested to focus on the implementing the architecture and leave the testing for the future projects, so it gave me more time to work on the other features. 

While I was working on the Network layer of the app I was many times blocked by the unresponsive server, so I've decided to create a Mock server for the future testing. It took me some time to build it but saved me much more since I was not dependant on backend any more. 

During the second coding period I have worked on the Create Election flow. At that point I felt more confident working on the TCA implementation, however I had more issues with the UI and Network support. I have started by aligning the create election model with the API, then introduced the TCA and plugged in the UI into it. At the end I've connected it to the network layer.

Since the topic was new to me I think the hardest part was to plan my work around it. As TCA requires a completely different structure of the app, and involves changing the networking and the navigation layer of the app, as well as a separate test store. 

I've realised the importance of prior planning when it comes to building an app's architecture as at times I felt the need to redo certain things and it'd take way more effort with multiple Pull Requests happening in parallel as ideally those features would have been split into multiple ones.

### Merge Requests

1. [Merge Request !1](https://gitlab.com/aossie/agora-ios/-/merge_requests/49) - Update project settings and frameworks  (**Merged**)
* Remove Carthage-related folders and files
* Remove Carthage build script from Target settings
* Add KVKCalendar and Realm frameworks to Swift Package Manager
* Lock Realm version to 10.28.1
* Re-add all frameworks as target dependencies

2. [Merge Request !2](https://gitlab.com/aossie/agora-ios/-/merge_requests/50) - Introduce The Composable Architecture   (**Merged**)
* Add package dependency via SPM

3. [Merge Request !3](https://gitlab.com/aossie/agora-ios/-/merge_requests/51) - Implement Sign-up flow using TCA   (**Merged**)
* Add support for Sign Up flow to unblock token generation (which will be used for all other API requests)
* Introduce TCA-driven architecture (reducers, state-management, actions, Store)
* Create finite state machine for the Sign Up flow
* Add Session loader to lay out the foundation for future session management
* Add tests to cover basic usage
* Add Unit tests to cover main scenarios: sign-up call, error response from the server, success response from the server.

4. [Merge Request !4](https://gitlab.com/aossie/agora-ios/-/merge_requests/52) - Implement Sign-in flow using TCA   (**Merged**)

Add support for the Sign-in to get access to refresh and authentication tokens:

* Rename authentication flow to account for both sign-up and sign-in;
* Update User model to support tokens;
* Clean up files and refactor classes;
* Replace the root screen with a Sign-in flow.


5. [Merge Request !5](https://gitlab.com/aossie/agora-ios/-/merge_requests/53) - Add support for a Mocked Server (**Finished, pending review from mentor**)

* Add an interface to abstract the network layer away
* Make URLSession conform to the API provider interface
* Add a mock that conforms to the interface too

6. [Merge Request !6](https://gitlab.com/aossie/agora-ios/-/merge_requests/54) - UI and business logic for authorization flows (**Merged**)

* Add a TCA-driven Navigation layer
* Add Landing view
* Replaced the Sign-in view with the new landing view
* Add a view for password reset
* Add Sign-up view
* Add a pop up view for the security questions
* Add a view for email sent confirmation
* Plug the UI into TCA


7. [Merge Requet !7](https://gitlab.com/aossie/agora-ios/-/merge_requests/55) - Align election model with API  (**Finished, pending review from mentor**)

* Address codestyle issues
* Add model for creating elections§

8. [Merge Request !8](https://gitlab.com/aossie/agora-ios/-/merge_requests/56) - Bind Create Election controls to TCA  (**Finished, pending review from mentor**)

* Align the UI with the updated election model
* Introduce TCA-driven architecture to the create election flow (Reducer, State-management, Actions, Store)
* Create finite state machine for the Create election flow
* Plug in the UI into TCA
* Deleted Realm DB integration from the screen

9. [Merge Request !9](https://gitlab.com/aossie/agora-ios/-/merge_requests/58) - Connect Create Election flow to the server (**Finished, pending review from mentor**)

* Integrate create election flow with the reducer
* Implement dependency injection of the authentication token throughout the app
* Introduce route builder and flow controller for the create election flow to allow switching between screens
* Add a success message modal view

10. [Merge Request !10](https://gitlab.com/aossie/agora-ios/-/merge_requests/57) - Add final project submission (**Finished, pending review from mentor**)

* Add Aleksandra.md file with GSoC'22 project details

### Future Scope

* Find an alternative way to implement navigation in the app ([issue 53!](https://gitlab.com/aossie/agora-ios/-/issues/53))
* Continue transition to the TCA: implement TCA in home view, settings view and the reast of election views
* Expand Network layer to connect remaining features to the server 
* Expand Test store
* Align the UI with the API and update accourding to the Human Interface Guidelines
