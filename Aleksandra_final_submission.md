# Agora Vote iOS

## Intern Name: Aleksandra Komagorkina ([@akomagorkina](https://gitlab.com/akomagorkina))
 
## Organization: [AOSSIE](https://aossie.gitlab.io/)

## [Agora iOS](https://gitlab.com/aossie/agora-ios/)

## [Source Code](https://gitlab.com/aossie/agora-ios/-/tree/gsoc-2022)

## Mentor: [Abhishek Agarwal](https://gitlab.com/ABHI165)

### Overview of the Application

Agora vote iOS allows users to create elections, invite voters, view results etc. This application uses Agora Web API as a backend application. It allows for elections to be held by using multiple algorithms such as Oklahoma, RangeVoting. The application has been built from scratch as a GSoC project in 2020.

### Goals

My project was focused on exploring and implementing The Composable Architecture (TCA) as well as continuing the implementation of SwiftUI while following the User Interface Guidelines and iOS best practices.

1.  [Migrate from Cathade to Swift Package Manage](https://gitlab.com/aossie/agora-ios/-/merge_requests/49) - **Done**
2.  [Introduce The Composable Architecture to the project](https://gitlab.com/aossie/agora-ios/-/merge_requests/50)- **Done**
3.  [Create finite state machine for the Sign Up flow](https://gitlab.com/aossie/agora-ios/-/merge_requests/51)- **Done**
4.  [Introduce Session loader to lay out the foundation for the session management](https://gitlab.com/aossie/agora-ios/-/merge_requests/51)- **Done**
5.  [Update User model to support tokens to align with the API](https://gitlab.com/aossie/agora-ios/-/merge_requests/52)- **Done**
6.  [Implement Sign-in flow using TCA](https://gitlab.com/aossie/agora-ios/-/merge_requests/52)- **Done**
7.  [Add a TCA-driven Navigation layer](https://gitlab.com/aossie/agora-ios/-/merge_requests/54) **Done**
8.  [Add a view for password reset](https://gitlab.com/aossie/agora-ios/-/merge_requests/54) **Done**
9.  [Add Sign-up view](https://gitlab.com/aossie/agora-ios/-/merge_requests/54) **Done**
10. [Add a view for email sent confirmation](https://gitlab.com/aossie/agora-ios/-/merge_requests/54) **Done**
11. [Complete the authorization flow](https://gitlab.com/aossie/agora-ios/-/merge_requests/54) **Done**
12. [Align election model with API](https://gitlab.com/aossie/agora-ios/-/merge_requests/55) **Done**
13. [Create finite state machine for the Create election flow](https://gitlab.com/aossie/agora-ios/-/merge_requests/56) **Done**
14. [ Bind Create Election controls to TCA](https://gitlab.com/aossie/agora-ios/-/merge_requests/56)**Done**


Enhancement feature:
[Add support for a Mocked Server](https://gitlab.com/aossie/agora-ios/-/merge_requests/53)**Done** - Sometimes the server fails to send a confirmation email. Introducing a way to Mock a server response would both unblock the developers and allow for future support in Unit tests.

### Summary of whole GSoC journey
​
I didn't have any prior contributions to AOSSIE’s or any other open source project so it was a completely new experience for me. Together with the mentor we decided to focus my progect on the integration of The Composable Architecture to the app starting with the authorization and create election flows as well as adding network support and updating the UI. 
​
In the first part of the coding period I've started integrating TCA to the app while working on the authorization flow. I have never worked with TCA before and was really happy to dedicate some to dive in into learning and exploring the topic. Instoducing a clear definition of State, Store, Action, Environment, and Reducers to the app was a wonderful idea since it allows to support better State management in SwiftUI. The app was missing the networking layer as well as a few screens. The introduction of the TCA required also required updating the navigation layer of the app. 
​
During the second coding period, I firstly finished the authorization flow. After that I started to work on the create election flow. First, I've aligned the create election model with the API, then introduced the TCA and pluged in the UI into it. 
​
Since the topic was new to me the hardest part was to plan my work around it. As TCA requires a completely different structure of the app, and involves changing the networking and the navigation layer of the app, as well as a separate Test store for testing. Those were things I've only realised whilealready working on the planned features, hence some of the merge requests are a bit long and ideally should have been splitted into a few separate ones.



### Merge Requests

1. [Merge Request !1](https://gitlab.com/aossie/agora-ios/-/merge_requests/49) - Update project settings and frameworks  (**Merged**)
* Remove Carthage-related folders and files
* Remove Carthage build script from Target settings
* Add KVKCalendar and Realm frameworks to Swift Package Manager
* Lock Realm version to 10.28.1
* Re-add all frameworks as target dependencies

2. [Merge Request !2](https://gitlab.com/aossie/agora-ios/-/merge_requests/50) - Introduce The Composable Architecture   (**Merged**)
* Add package dependency via SPM

3. [Merge Request !3](https://gitlab.com/aossie/agora-ios/-/merge_requests/51) - Implement Sign-up flow using TCA   (**Merged**)
* Add support for Sign Up flow to unblock token generation (which will be used for all other API requests)
* Introduce TCA-driven architecture (reducers, state-management, actions, Store)
* Create finite state machine for the Sign Up flow
* Add Session loader to lay out the foundation for future session management
* Add tests to cover basic usage
* Add Unit tests to cover main scenarios: sign-up call, error response from the server, success response from the server.

4. [Merge Request !4](https://gitlab.com/aossie/agora-ios/-/merge_requests/52) - Implement Sign-in flow using TCA   (**Merged**)

Add support for the Sign-in to get access to refresh and authentication tokens:

* Rename authentication flow to account for both sign-up and sign-in;
* Update User model to support tokens;
* Clean up files and refactor classes;
* Replace the root screen with a Sign-in flow.


5. [Merge Request !5](https://gitlab.com/aossie/agora-ios/-/merge_requests/53) - Add support for a Mocked Server (**Finished, pending review from mentor**)

* Add an interface to abstract the network layer away
* Make URLSession conform to the API provider interface
* Add a mock that conforms to the interface too

6. [Merge Request !6](https://gitlab.com/aossie/agora-ios/-/merge_requests/54) - UI and business logic for authorization flows (**Finished, pending review from mentor**)

* Add a TCA-driven Navigation layer
* Add Landing view
* Replaced the Sign-in view with the new landing view
* Add a view for password reset
* Add Sign-up view
* Add a pop up view for the security questions
* Add a view for email sent confirmation
* Plug the UI into TCA


7. [Merge Requet !7](https://gitlab.com/aossie/agora-ios/-/merge_requests/55) - Align election model with API  (**Finished, pending review from mentor**)
* Address codestyle issues
* Add model for creating elections

8. [Merge Request !8](https://gitlab.com/aossie/agora-ios/-/merge_requests/56)- Bind Create Election controls to TCA  (**Finished, pending review from mentor**)
* Align the UI with the updated election model
* Introduce TCA-driven architecture to the create election flow (Reducer, State-management, Actions, Store)
* Create finite state machine for the Create election flow
* Plug in the UI into TCA
* Deleted Realm DB integration from the screen

9. [Merge Request !9]() Connect Create election flow to the server **Finished, pending review from mentor**)

10.[Merge Request !10](https://gitlab.com/aossie/agora-ios/-/merge_requests/57) Add final project submission
* Add Aleksandra.md file with GSoC'22 project details

### Future Scope

* Migrating the rest of the app to the TCA 
* Adding server support for the remaining features
* Expanding Test store
* Update the UI to follow Human Interface Guidelines.
