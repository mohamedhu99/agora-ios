//
//  AuthRoute.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 03.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import SwiftUI
import ComposableArchitecture

enum AuthRoute: Equatable {
    case signUp
    case signIn
    case securityQuestion
    case home
    case emailSent
    case forgotPassword
    case errorModal(String)
    
    case previousScreen
    case initial
}

