//
//  AuthEnvironment.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 08.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation
import ComposableArchitecture

struct AuthEnvironment {
    var mainQueue: AnySchedulerOf<DispatchQueue>
    var uuid: () -> UUID
    let authBusinessController: SessionLoader
    var flowController: AuthFlowController
}
