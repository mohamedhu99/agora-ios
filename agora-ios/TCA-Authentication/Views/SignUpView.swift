//
//  SignUpView.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 08.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import SwiftUI
import ComposableArchitecture

typealias AuthStore = Store<AuthState, AuthAction>

struct SignUpView: View {
    let store: AuthStore
    
    var body: some View {
        WithViewStore(store) { viewStore in
            ScrollView {
                VStack(alignment: .leading, spacing: 40) {
                    Spacer()
                    VStack (spacing: 40) {
                        VStack(alignment: .leading, spacing: 5)  {
                            TextField(
                                "Username",
                                text: Binding<String>(
                                    get: { viewStore.state.signUpDetails.identifier },
                                    set: { newValue in
                                        viewStore.send(.userTypedUsername(newValue.lowercased()))
                                    }
                                )
                            )
                            Divider()
                        }
                        
                        HStack(spacing: 20){
                            VStack (alignment: .leading, spacing: 5) {
                                TextField(
                                    "First Name",
                                    text: Binding<String>(
                                        get: { viewStore.state.signUpDetails.firstName },
                                        set: { newValue in
                                            viewStore.send(.userTypedFirstName(newValue))
                                        }
                                    )
                                )
                                Divider()
                            }
                            Spacer()
                            VStack (alignment: .trailing, spacing: 5) {
                                TextField(
                                    "Last Name",
                                    text: Binding<String>(
                                        get: { viewStore.state.signUpDetails.lastName },
                                        set: { newValue in
                                            viewStore.send(.userTypedLastName(newValue))
                                        }
                                    )
                                )
                                Divider()
                            }
                        }
                        
                        VStack(alignment: .leading, spacing: 5) {
                            TextField(
                                "Email",
                                text: Binding<String>(
                                    get: { viewStore.state.signUpDetails.email },
                                    set: { newValue in
                                        viewStore.send(.userTypedEmail(newValue.lowercased()))
                                    }
                                )
                            )
                            Divider()
                        }
                        
                        VStack(alignment: .leading, spacing: 5) {
                            SecureField (
                                "Password",
                                text: Binding<String>(
                                    get: { viewStore.state.signUpDetails.password },
                                    set: { newValue in
                                        viewStore.send(.userTypedPassword(newValue))
                                    }
                                )
                            )
                            Divider()
                        }
                        VStack(alignment: .leading, spacing: 5) {
                            Button {
                                viewStore.send(.showSecurityQuestion)
                            } label: {
                                HStack {
                                    Text(
                                        viewStore.state.userSecurityQuestion.isEmpty
                                        ? "Secret Security Question"
                                        : viewStore.state.userSecurityQuestion
                                    )
                                    Spacer()
                                    Image(systemName: "chevron.right")
                                }
                            }.foregroundColor(Color.gray)
                            Divider()
                        }
                        VStack(alignment: .leading, spacing: 5) {
                            TextField(
                                "Please write your answer here",
                                text: Binding<String>(
                                    get: { viewStore.state.signUpDetails.securityQuestion.answer },
                                    set: { newValue in
                                        viewStore.send(.userTypedSequrityQuestionAnswer(newValue))
                                    }
                                )
                            )
                            Divider()
                        }
                    }
                    
                    Button("Sign Up") {
                        viewStore.send(.signUpPressed)
                    }
                    .foregroundColor(Color.white)
                    .frame(maxWidth: .infinity, minHeight: 40)
                    .background(Color.gray)
                    .cornerRadius(15)
                }
                .padding()
            }
            .onTapGesture {
                hideKeyboard()
            }
            
        }
    }
}


