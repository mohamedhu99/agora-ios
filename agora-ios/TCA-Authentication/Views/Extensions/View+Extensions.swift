//
//  View+Extensions.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 19.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import UIKit
import SwiftUI

extension View {
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
