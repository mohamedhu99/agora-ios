//
//  LandingView.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 09.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import SwiftUI
import ComposableArchitecture

struct LandingView: View {
    let store: AuthStore
    
    var body: some View {
        WithViewStore(store) { viewStore in
            VStack {
                VStack(alignment: .leading, spacing: 40) {
                    Text("Login")
                        .font(.title.bold())
                    VStack(alignment: .leading, spacing: 15)  {
                        TextField(
                            "Username",
                            text: Binding<String>(
                                get: { viewStore.state.username },
                                set: { newValue in
                                    viewStore.send(.userTypedUsername(newValue.lowercased()))
                                }
                            )
                        )
                        Divider()
                    }
                    VStack(alignment: .leading, spacing: 5) {
                        SecureField (
                            "Password",
                            text: Binding<String>(
                                get: {  viewStore.state.password  },
                                set: { newValue in
                                    viewStore.send(.userTypedPassword(newValue.lowercased()))
                                }
                            )
                        )
                        Divider()
                    }
                }
                .padding(20)
                HStack {
                    Spacer()
                    Button("Forgot Password?") {
                        viewStore.send(.forgotPasswordPressed)
                    }
                    .font(.subheadline)
                    .foregroundColor(.blue)
                }
                .padding()
                VStack {
                    Button("Sign In") {
                        viewStore.send(.logInPressed)
                    }
                    .disabled(viewStore.state.isNextDisabled)
                    .foregroundColor(viewStore.state.isNextDisabled ? Color.gray : Color.black)
                    .font(.headline.bold())
                    .frame(maxWidth: .infinity, minHeight: 40)
                    .background(Color.yellow)
                    .cornerRadius(15)
                    
                    LabelledDivider(label: "or")
                    
                    Button("Create account") {
                        viewStore.send(.showCreateAccount)
                    }
                    .foregroundColor(Color.white)
                    .font(.headline.bold())
                    .frame(maxWidth: .infinity, minHeight: 40)
                    .background(Color.gray)
                    .cornerRadius(15)
                }
                .padding()
            }
            .onTapGesture {
                hideKeyboard()
            }
        }
    }
}
