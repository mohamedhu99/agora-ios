//
//  ForgotPasswordView.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 11.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import SwiftUI
import ComposableArchitecture

struct ForgotPasswordView: View {
    let store: AuthStore
    
    var body: some View {
        WithViewStore(store) { viewStore in
            VStack(alignment: .leading, spacing: 20) {
                Text("Please enter user name to receive password reset link")
                    .font(.headline)
                TextField(
                    "User Name",
                    text: Binding<String>(
                        get: { viewStore.state.username },
                        set: { newValue in
                            viewStore.send(.userTypedUsername(newValue.lowercased()))
                        }
                    )
                )
                Divider()
                Button("Send link") {
                    viewStore.send(.sendLinkPressed)
                }
                .foregroundColor(Color.white)
                .frame(maxWidth: .infinity, minHeight: 40)
                .background(Color.gray)
                .cornerRadius(15)
            }
            .onTapGesture {
                hideKeyboard()
            }
        }
        .padding(20)
    }
}
