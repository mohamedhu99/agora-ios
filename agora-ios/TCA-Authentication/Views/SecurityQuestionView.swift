//
//  SecurityQuestionView.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 03.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import SwiftUI
import ComposableArchitecture

struct SecurityQuestionView: View {
    var store: AuthStore
    
    
    var body: some View {
        WithViewStore(store) { viewStore in
            VStack {
                Spacer()
                List(viewStore.state.questions, id: \.self) { question in
                    SelectableRow(text: question)
                        .onTapGesture {
                            viewStore.send(.userSelected(question))
                        }
                }
                Button() {
                    viewStore.send(.dismiss)
                } label: {
                    Image(systemName: "x.circle")
                        .font(.largeTitle)
                }
                .font(.title)
                .padding()
            }
        }
    }
}




