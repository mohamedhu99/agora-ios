//
//  EmailSentView.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 11.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import SwiftUI
import ComposableArchitecture

struct EmailSentView: View {
    let store: AuthStore
    
    var body: some View {
        WithViewStore(store) { viewStore in
            VStack(alignment: .center){
                    Text("Please check your email and follow the instructions.")
            }
            .padding(10)
        }
    }
}
    

