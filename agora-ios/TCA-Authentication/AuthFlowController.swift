//
//  AuthFlowController.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 03.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import UIKit
import SwiftUI
import ComposableArchitecture

class AuthFlowController {
    var builder: AuthBuilder?
    
    func handle(_ route: AuthRoute) {
        guard let builder = builder else { return }
        
        switch route {
        case .securityQuestion:
            builder.rootViewController?.present(
                builder.makeSecurityQuestionView(),
                animated: true,
                completion: nil
            )
        case .previousScreen:
            builder.rootViewController?
                .presentedViewController?
                .dismiss(animated: true, completion: nil)
        case .home:
            let windowsScene = builder.rootViewController?.view.window?.windowScene
            
            if let windowScene = windowsScene {
                let window = UIWindow(windowScene: windowScene)
                window.rootViewController = UIHostingController(rootView: Navigation())
                (builder.rootViewController?.view.window?.windowScene?.delegate as? SceneDelegate)?.window = window
                window.makeKeyAndVisible()
            }
            
        case .emailSent:
            builder
                .rootViewController?
                .navigationController?
                .pushViewController(
                    builder.makeEmailSentView(),
                    animated: true
            )
        
        case .signUp:
            builder
                .rootViewController?
                .navigationController?
                .pushViewController(
                    builder.makeSignUpView(),
                    animated: true
            )
            
        case .forgotPassword:
            builder
                .rootViewController?
                .navigationController?
                .pushViewController(
                    builder.makeForgotPasswordView(),
                    animated: true
            )
        case let .errorModal(errorMessage):
            builder.rootViewController?.present(
                builder.makeErrorModalView(errorMessage: errorMessage),
                animated: true,
                completion: nil
            )

        default:
            break
        }
    }
}
