//
//  AuthAction.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 08.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation
import ComposableArchitecture


enum AuthAction: Equatable {
    case showCreateAccount
    case signUpPressed // create user
    case userLoaded(User) // fetch user
    case serverResponded(String)
    case authFailed(String)
    case userTypedUsername(String)
    case userTypedPassword(String)
    case userTypedFirstName(String)
    case userTypedLastName(String)
    case userTypedEmail(String)
    case userTypedSequrityQuestionAnswer(String)
    case userSelected(String)
    case logInPressed
    case forgotPasswordPressed
    case sendLinkPressed
    case dismiss
    case showSecurityQuestion
    case emailSent
}
