//
//  AuthReducer.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 08.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import SwiftUI
import ComposableArchitecture
import RealmSwift

let authReducer = Reducer<AuthState, AuthAction, AuthEnvironment> { state, action, environment in
    func isValidCredentials() -> Bool {
        state.password.isEmpty == false && state.username.isEmpty == false
    }
    
    func isValidSignUpDetails() -> Bool {
        state.signUpDetails.identifier.isEmpty == false && state.signUpDetails.email.isEmpty == false && state.signUpDetails.password.isEmpty == false
    }
    
    switch action {
    case .logInPressed:
        let credentials = Credentials(identifier: state.username, password: state.password, trustedDevice: "yes")
        return environment
            .authBusinessController
            .signIn(withCredentials: credentials)
            .receive(on: environment.mainQueue)
            .catchToEffect { result in
                switch result {
                case let .success(user):
                    return .userLoaded(user)
                case let .failure(error):
                    let errorMessage = (error as? ErrorResponse)?.errorMessage ?? error.localizedDescription
                    return .authFailed(errorMessage)
                }
            }
        
    case let .userTypedUsername(username):
        switch state.currentScreen {
        case .forgotPassword:
            state.username = username
        case .signIn:
            state.username = username
            state.isNextDisabled = isValidCredentials() == false
        case .signUp:
            state.signUpDetails.identifier = username
            state.isNextDisabled = isValidSignUpDetails() == false
        default:
            break
        }
        
    case let .userTypedPassword(password):
        switch state.currentScreen {
        case .signIn:
            state.password = password
            state.isNextDisabled = isValidCredentials() == false
        case .signUp:
            state.signUpDetails.password = password
            state.isNextDisabled = isValidSignUpDetails() == false
        default: break
        }
        
    case let .userTypedFirstName(firstName):
        state.signUpDetails.firstName = firstName
        state.isNextDisabled = isValidSignUpDetails() == false
        
    case let .userTypedLastName(lastName):
        state.signUpDetails.lastName = lastName
        state.isNextDisabled = isValidSignUpDetails() == false
        
    case let .userTypedEmail(email):
        state.signUpDetails.email = email
        state.isNextDisabled = isValidSignUpDetails() == false
        
    case let .userTypedSequrityQuestionAnswer(answer):
        state.signUpDetails.securityQuestion.answer = answer
        state.isNextDisabled = isValidSignUpDetails() == false

    case .showSecurityQuestion:
        state.nextScreen = .securityQuestion
  
//    case let .show(route):
//        state.currentScreen = route
//        environment.flowController.handle(state.currentScreen)
        
    case .signUpPressed:
        return environment
            .authBusinessController
            .signUp(withUser: state.signUpDetails)
            .receive(on: environment.mainQueue)
            .catchToEffect { result in
                switch result {
                case let .success(message):
                    return .serverResponded(message)
                case let .failure(error):
                    let errorMessage = (error as? ErrorResponse)?.errorMessage ?? error.localizedDescription
                    return .authFailed(errorMessage)
                }
            }
        
    case let .userLoaded(user):
        state.userMessage = "Signed in successfully! Email: \(user.email)"
        state.nextScreen = .home
        
    case let .serverResponded(message):
        state.userMessage = message
        switch state.currentScreen {
        case .signUp, .forgotPassword:
           return Effect(value: .emailSent)
        default:
            break
        }
        
    case let .authFailed(errorMessage):
        state.userMessage = errorMessage
        environment.flowController.handle(.errorModal(errorMessage))
        return .none
        
    case .dismiss:
        _ = state.history.popLast()
        environment.flowController.handle(.previousScreen)
        return .none
        
    case .emailSent:
        state.nextScreen = .emailSent
        
    case .showCreateAccount:
        state.nextScreen = .signUp
        
    case let .userSelected(question):
        state.signUpDetails.securityQuestion.question = question
        return Effect(value: .dismiss)
        
    case .forgotPasswordPressed:
        state.nextScreen = .forgotPassword
        
    case .sendLinkPressed:
        return environment
            .authBusinessController
            .resetPassword(withUserName: state.username)
            .receive(on: environment.mainQueue)
            .catchToEffect { result in
                switch result {
                case let .success(message):
                    return .serverResponded(message)
                case let .failure(error):
                    let errorMessage = (error as? ErrorResponse)?.errorMessage ?? error.localizedDescription
                    return .authFailed(errorMessage)
                }
            }
        
    }
    
    if let nextScreen = state.nextScreen {
        state.history.append(nextScreen)
        state.nextScreen = nil
        environment.flowController.handle(nextScreen)
    }
    
    return .none
}


