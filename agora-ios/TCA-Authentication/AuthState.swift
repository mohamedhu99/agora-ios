//
//  AuthState.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 08.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation
import ComposableArchitecture

struct AuthState: Equatable {
    var signUpDetails: SignUpDetails
    var userMessage: String = "Please sign up"
    
    var username: String = ""
    var email: String = ""
    var password: String = ""
    
    var isNextDisabled = true
    
    var history: [AuthRoute] = []
    var nextScreen: AuthRoute?
    
    var userSecurityQuestion: String {
        signUpDetails.securityQuestion.question
    }
    
    var currentScreen: AuthRoute? {
        history.last
    }
    
    let questions: [String]
}
