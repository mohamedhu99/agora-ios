//
//  AuthBuilder.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 08.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import SwiftUI
import ComposableArchitecture

class AuthBuilder {
    var rootViewController: UIViewController?
    let store: Store<AuthState, AuthAction>
    let authFlowController: AuthFlowController
    
    init() {
        self.authFlowController = AuthFlowController()
        let authEnvironment = AuthEnvironment(
            mainQueue: .main,
            uuid: UUID.init,
            authBusinessController: SessionLoader(),
            flowController: self.authFlowController
//            authBusinessController: SessionLoader(urlSession: MockAPIServer())
        )
        
        
        self.store = Store<AuthState, AuthAction>.init(
            initialState: .init(
                signUpDetails: SignUpDetails.init(
                    identifier: "",
                    password: "",
                    email: "",
                    firstName: "",
                    lastName: "",
                    securityQuestion: .init(
                        crypto: "",
                        question: "",
                        answer: "")
                ),
                history: [.signIn],
                questions: [
                    "What is the name of your favorite pet?",
                    "What is your mother's maiden name?",
                    "What high school did you attend?",
                    "What was the name of your elementary school?",
                    "What was your favorite food as a child?",
                    "In what city were you born?"
                ]
            ),
            reducer: authReducer,
            environment: authEnvironment
        )
    }
    
    func makeSignUpView() -> UIViewController {
        let view = SignUpView(store: store)
        let hosting = UIHostingController(rootView: view)
        hosting.title = "Sign Up"
        return hosting
    }
    
    func makeEmailSentView() -> UIViewController {
        let view = EmailSentView(store: store)
        let hosting = UIHostingController(rootView: view)
        return hosting
    }
    
    func makeErrorModalView(errorMessage: String) -> UIAlertController {
        let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
        alert.addAction(
            UIAlertAction(
                title: "OK",
                style: .default,
                handler: { _ in }
            )
        )
        return alert
    }
    
    func makeForgotPasswordView() -> UIViewController {
        let view = ForgotPasswordView(store: store)
        let hosting = UIHostingController(rootView: view)
        return hosting
    }
    
    func makeSecurityQuestionView() -> UIViewController {
        let view = SecurityQuestionView(store: store)
        let hosting = UIHostingController(rootView: view)
        return hosting
    }
    
    func makeLandingView() -> UIViewController {
        let view = LandingView(store: store)
        
        let hosting = UIHostingController(rootView: view)
        rootViewController = hosting
        authFlowController.builder = self
        
        let navigationController = UINavigationController(
            rootViewController: hosting
        )
        navigationController.navigationBar.prefersLargeTitles = true
        
        return navigationController
    }

}
