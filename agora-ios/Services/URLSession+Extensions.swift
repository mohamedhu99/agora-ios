//
//  URLSession+Extensions.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 21.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation
import Combine

extension URLSession: APIProvider {
    func response(for request: URLRequest) -> AnyPublisher<APIResponse, URLError> {
        dataTaskPublisher(for: request).eraseToAnyPublisher()
    }
}
