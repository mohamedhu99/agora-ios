//
//  Network.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 25.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation
import Combine

// MARK: - Network
struct Network {
    static let baseUrl = "https://agora-rest-api.herokuapp.com/"
    var urlSession = URLSession.shared

    func requestBuilder<T: Encodable>(path: String, object: T) -> URLRequest {
        guard let url = URL(string: Network.baseUrl + path) else {
            fatalError("Base URL is invalid")
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "accept")
        
        if let requestBody = try? JSONEncoder().encode(object) {
            request.httpBody = requestBody
        }
        return request
    }
}

//MARK: - Parsing publisher

extension URLSession {
    func publisher<T: Decodable>(
        for request: URLRequest,
        responseType: T.Type = T.self,
        decoder: JSONDecoder = .init()
    ) -> AnyPublisher<T, Error> {
        dataTaskPublisher(for: request)
            .tryMap { (data, response) -> T in
            let decoder = JSONDecoder()
            if let successMessage = try? decoder.decode(T.self, from: data) {
                return successMessage
            }
            throw try decoder.decode(ErrorResponse.self, from: data)
        }
        .eraseToAnyPublisher()
    }
}
