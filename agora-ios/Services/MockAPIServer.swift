//
//  MockAPIServer.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 21.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation
import Combine

struct MockAPIServer: APIProvider {
    func response(for request: URLRequest) -> AnyPublisher<APIResponse, URLError> {
        let successMessage = "Signed up successfully!"
        let successData = successMessage.data(using: .utf8)!
        let response: URLResponse = HTTPURLResponse(
            url: request.url!,
            statusCode: 200,
            httpVersion: "HTTP/1.1",
            headerFields: nil
        )!
        
        guard let endOfUrl = request.url?.absoluteString.split(separator: "/").last else {
            return Result.Publisher((data: successData, response: response))
                .eraseToAnyPublisher()
        }
        
        switch endOfUrl {
        case "login":
            let user = User(
                username: "test",
                email: "test@test.com",
                firstName: "Test",
                lastName: "Test",
                avatarURL: "Test",
                twoFactorAuthentication: true,
                authToken: .init(
                    token: "",
                    expiresOn: ""
                ),
                refreshToken: .init(
                    token: "",
                    expiresOn: ""
                )
            )
            let jsonEncoder = JSONEncoder()
            let encodedUser = try! jsonEncoder.encode(user)
            return Result.Publisher((data: encodedUser, response: response))
                .eraseToAnyPublisher()
        default:
            return Result.Publisher((data: successData, response: response))
                        .eraseToAnyPublisher()
        }
    }
}
