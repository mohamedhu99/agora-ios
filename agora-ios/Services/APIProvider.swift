//
//  APIProvider.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 21.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation
import Combine

protocol APIProvider {
    typealias APIResponse = URLSession.DataTaskPublisher.Output
    func response(for request: URLRequest) -> AnyPublisher<APIResponse, URLError>
}
