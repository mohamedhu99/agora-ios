//
//  SessionLoader.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 08.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation
import Combine
import Alamofire

struct SessionLoader {
    var urlSession: APIProvider
    
    init(urlSession: APIProvider = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    func signUp(withUser user: SignUpDetails) -> AnyPublisher<String, Error> {
        let url = URL(string: Network.baseUrl + "api/v1/auth/signup")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "accept")
        
        if let requestBody = try? JSONEncoder().encode(user) {
            request.httpBody = requestBody
        }
        
        return urlSession
            .response(for: request)
            .tryMap { (data, response) -> String in
                let decoder = JSONDecoder()
                if let message = String(data: data, encoding: .utf8) {
                    return message
                }
                throw try decoder.decode(ErrorResponse.self, from: data)
            }
            .eraseToAnyPublisher()
    }
    
    func signIn(withCredentials credentials: Credentials) -> AnyPublisher<User, Error> {
        let url = URL(string: Network.baseUrl + "api/v1/auth/login")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "accept")
        
        if let requestBody = try? JSONEncoder().encode(credentials) {
            request.httpBody = requestBody
        }
        
        return urlSession
            .response(for: request)
            .tryMap { (data, response) -> User in
                let decoder = JSONDecoder()
                if let user = try? decoder.decode(User.self, from: data) {
                    return user
                }
                throw try decoder.decode(ErrorResponse.self, from: data)
            }
            .eraseToAnyPublisher()
    }
    
    func resetPassword(withUserName userName: String) -> AnyPublisher<String, Error> {
        let url = URL(string: Network.baseUrl + "api/v1/auth/forgotPassword/send/\(userName)")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "accept")
        
        return urlSession
            .response(for: request)
            .tryMap { (data, response) -> String in
                let decoder = JSONDecoder()
                if let message = String(data: data, encoding: .utf8) {
                    return message
                }
                throw try decoder.decode(ErrorResponse.self, from: data)
            }
            .eraseToAnyPublisher()
    }
    
    func mockLogInSuccessResponce() {
        
    }
}
