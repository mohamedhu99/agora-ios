//
//  SelectableRow.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 11.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import SwiftUI

struct SelectableRow: View {
    var text: String

    var body: some View {
        Text(text)
    }
}
