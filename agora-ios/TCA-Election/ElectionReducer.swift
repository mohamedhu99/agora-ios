//
//  ElectionReducer.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 27.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation
import ComposableArchitecture

let  electionReducer = Reducer<ElectionState, ElectionAction, ElectionEnvironment> { state, action, environment in
    
    switch action {
    case .didAppear:
        break
    case .dismissElectionType:
        state.isElectionTypePresented = false
    case let .setElectionTypePresented(isPresented):
        state.isElectionTypePresented = isPresented
    case let .userSelected(electionType):
        state.election.electionType = electionType
        state.isElectionTypePresented = false
    case let .userDidTypeName(name):
        state.election.name = name
    case let .userDidTypeDescription(description):
        state.election.description = description
    case let .userDidTypeCandidates(candidates):
        // "a,b,c" ->
        state.election.candidates = candidates
            .split(separator: ",") // [Substring("a"), "b", "c"]
            .map { String($0) } // [String("a", ...]
    case let .userDidToggleVoteListVisibility(visibility):
        state.election.voterListVisibility = visibility
    case let .userDidChooseStartingDate(date):
        state.election.startingDate = date
    case let .userDidChooseEndingDate(date):
        state.election.endingDate = date
    case .userDidTapCreateElection:
        print("Save election")
    case let .userDidToggleInvite(invite):
        state.election.isInvite = invite
    case let .userDidToggleRealTime(realTime):
        state.election.isRealTime = realTime
   
    }
    
    return .none
}
