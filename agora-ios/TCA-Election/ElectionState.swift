//
//  ElectionState.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 27.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import SwiftUI
import ComposableArchitecture

struct ElectionState: Equatable {
    var election: CreateElection
    var electionTypes: [String]
    var isElectionTypePresented = false
    
    var candidateNames: String {
        election
            .candidates // ["a", "b", "c"]
            .joined(separator: ",") // "abc" -> "a,b,c"
    }
}
