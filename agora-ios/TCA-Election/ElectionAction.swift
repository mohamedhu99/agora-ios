//
//  ElectionAction.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 27.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation

enum ElectionAction: Equatable {
    case didAppear
    case userDidTapCreateElection
    case userDidTypeName(String)
    case userDidTypeDescription(String)
    case userDidTypeCandidates(String)
    case setElectionTypePresented(Bool)
    case dismissElectionType
    case userSelected(String)
    case userDidToggleVoteListVisibility(Bool)
    case userDidChooseStartingDate(Date)
    case userDidChooseEndingDate(Date)
    case userDidToggleInvite(Bool)
    case userDidToggleRealTime(Bool)
}
