//
//  ElectionType.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 28.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation

struct ElectionType: Identifiable {
    let id = UUID()
    let question: String
}
