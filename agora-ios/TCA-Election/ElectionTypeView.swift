//
//  ElectionTypeView.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 27.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import SwiftUI
import ComposableArchitecture

struct ElectionTypeView: View {
    var store: ElectionStore
    
    
    var body: some View {
        WithViewStore(store) { viewStore in
            VStack {
                Spacer()
                List(viewStore.state.electionTypes, id: \.self) { election in
                    SelectableRow(text: election)
                        .onTapGesture {
                            viewStore.send(.userSelected(election))
                        }
                }
                Button() {
                    viewStore.send(.dismissElectionType)
                } label: {
                    Image(systemName: "x.circle")
                        .font(.largeTitle)
                }
                .font(.title)
                .padding()
            }
        }
    }
}
