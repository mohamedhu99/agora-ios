//
//  ElectionFlowController.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 27.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import UIKit
import SwiftUI
import ComposableArchitecture

class ElectionFlowController {
    var builder: ElectionBuilder?
    
    func handle(_ route: ElectionRoute) {
        guard let builder = builder else { return }
        
        switch route {
        case .showElectionType:
            builder.rootViewController?.present(
                builder.makeElectionTypeView(),
                animated: true,
                completion: nil
            )

        default:
            break
        }
    }
}
