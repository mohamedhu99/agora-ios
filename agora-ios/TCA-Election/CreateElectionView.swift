import SwiftUI
import RealmSwift
import ComposableArchitecture

typealias ElectionStore = Store<ElectionState, ElectionAction>

struct CreateElectionView: View {
    var store: ElectionStore
    
    var body: some View {
        ZStack {
            VStack {
                Mid_Elections(store: store)
                    .navigationBarTitle("New Election", displayMode: .inline)
            }
        }
        .background(with: Color("backgroundColor"))
    }
}

struct Mid_Elections: View {
    
    var store: ElectionStore
    
    @State var description: String = ""
    @State private var endingDate = Date()
    
    @State private var wakeUp = Date()
    @State var isStart: Bool = false
    @State var isEnd: Bool = false
    
    var body: some View {
        WithViewStore(store) { viewStore in
            VStack {
                VStack {
                    ZStack {
                        VStack {
                            LinearGradient(
                                gradient: Gradient(
                                    colors: [
                                        Color("Color2_2"),
                                        Color("Color2")
                                    ]
                                ),
                                startPoint: .bottom,
                                endPoint: .top)
                            .frame(
                                width: UIScreen.main.bounds.width,
                                height: 50,
                                alignment: .center
                            )
                        }
                    }
                    ScrollView {
                        VStack {
                            HStack {
                                Image(systemName: "pencil")
                                    .padding()
                                TextField(
                                    "Title",
                                    text: Binding<String>(
                                        get: { viewStore.election.name },
                                        set: { newValue in
                                            viewStore.send(.userDidTypeName(newValue))
                                        }
                                    )
                                )
                            }
                            Divider().frame(
                                minWidth: 0,
                                maxWidth: 320,
                                alignment: .center
                            )
                            HStack {
                                Image(systemName: "square.and.pencil")
                                    .padding()
                                TextField(
                                    "Description",
                                    text: Binding<String>(
                                        get: { viewStore.election.description },
                                        set: { newValue in
                                            viewStore.send(.userDidTypeDescription(newValue))
                                        }
                                    )
                                )
                            }
                            Divider().frame(
                                minWidth: 0,
                                maxWidth: 320,
                                alignment: .center
                            )
                            HStack {
                                Image(systemName: "person")
                                    .padding()
                                TextField(
                                    "Candidates",
                                    text: Binding<String>(
                                        get: { viewStore.candidateNames },
                                        set: { newValue in
                                            viewStore.send(.userDidTypeCandidates(newValue))
                                        }
                                    )
                                )
                            }
                        }
                        .background(Color(.white))
                        .cornerRadius(10)
                        VStack {
                            HStack {
                                Image(systemName: "list.bullet.rectangle.portrait").padding()
                                Button {
                                    viewStore.send(.setElectionTypePresented(true))
                                } label: {
                                    HStack {
                                        Text(
                                            viewStore.state.election.electionType.isEmpty
                                            ? "Choose election type"
                                            : viewStore.state.election.electionType
                                        )
                                        Spacer()
                                        Image(systemName: "chevron.right")
                                    }
                                }.foregroundColor(Color.gray)
                            }
                            Divider().frame(
                                minWidth: 0,
                                maxWidth: 320,
                                alignment: .center
                            )
                            HStack {
                                Image(systemName: "eye.square").padding()
                                //checkbox
                                Button (action: {
                                    viewStore.send(
                                        .userDidToggleVoteListVisibility(
                                            !viewStore.election.voterListVisibility
                                        )
                                    )
                                }) {
                                    HStack {
                                        Text("Voter List Visability")
                                        Spacer()
                                        Image(
                                            systemName: viewStore.election.voterListVisibility ? "checkmark.square.fill": "square"
                                        )
                                        .resizable()
                                        .foregroundColor(Color("Color2"))
                                        .frame(
                                            width: 16,
                                            height: 16,
                                            alignment: .trailing
                                        )
                                        .padding()
                                    }
                                }
                            }
                        } .background(Color(.white))
                            .frame(minWidth: 0,
                                   maxWidth: 380,
                                   alignment: .center
                            )
                            .cornerRadius(10)
                        
                            VStack {
                                HStack {
                                    if (self.isStart) {
                                        toggleButton(givenStateObject: $isStart, outputString: "Done")
                                    } else if (self.isStart == false) {
                                        Image(systemName: "clock")
                                            .padding()
                                        toggleButton(givenStateObject: $isStart, outputString: "Start: \( viewStore.state.election.startingDate)")
                                    }
                                    VStack {
                                        if (self.isStart) {
                                            DatePicker(
                                                "Start",
                                                selection: Binding<Date>.init(
                                                    get: {
                                                        viewStore.election.startingDate
                                                    },
                                                    set: { newDate in
                                                        viewStore.send(.userDidChooseStartingDate(newDate))
                                                    }
                                                ),
                                                in: Date()...
                                            ).labelsHidden()
                                        }
                                    }
                                }
                                Divider()
                                    .frame(
                                        minWidth: 0,
                                        maxWidth: 320,
                                        alignment: .center
                                    )
                                HStack {
                                    if (self.isEnd) {
                                        toggleButton(givenStateObject: $isEnd, outputString: "Done")
                                    } else if (self.isEnd == false) {
                                        Image(systemName: "clock")
                                            .padding()
                                        toggleButton(givenStateObject: $isEnd, outputString: "End: \( viewStore.state.election.endingDate)")
                                    }
                                    VStack {
                                        if (self.isEnd) {
                                            DatePicker(
                                                "Start",
                                                selection: Binding<Date>.init(
                                                    get: {
                                                        viewStore.election.endingDate
                                                    },
                                                    set: { newDate in
                                                        viewStore.send(.userDidChooseEndingDate(newDate))
                                                    }
                                                ),
                                                in: Date()...
                                            ).labelsHidden()
                                        }
                                    }
                                }
                            Divider()
                                .frame(
                                    minWidth: 0,
                                    maxWidth: 320,
                                    alignment: .center
                                )
                            VStack {
                                HStack {
                                    Image(systemName: "globe").padding()
                                    //checkbox
                                    Button (action: {
                                        viewStore.send(
                                            .userDidToggleRealTime(
                                                !viewStore.election.isRealTime
                                            )
                                        )
                                    }) {
                                        HStack {
                                            Text("Real Time")
                                            Spacer()
                                            Image(
                                                systemName: viewStore.election.isRealTime ? "checkmark.square.fill": "square"
                                            )
                                            .resizable()
                                            .foregroundColor(Color("Color2"))
                                            .frame(
                                                width: 16,
                                                height: 16,
                                                alignment: .trailing
                                            )
                                            .padding()
                                        }
                                    }
                                }
                                Divider()
                                    .frame(
                                        minWidth: 0,
                                        maxWidth: 320,
                                        alignment: .center
                                    )
                                HStack {
                                    Image(systemName: "square.and.arrow.up").padding()
                                    //checkbox
                                    Button (action: {
                                        viewStore.send(
                                            .userDidToggleInvite(
                                                !viewStore.election.isInvite
                                            )
                                        )
                                    }) {
                                        HStack {
                                            Text("Invite")
                                            Spacer()
                                            Image(
                                                systemName: viewStore.election.isInvite ? "checkmark.square.fill": "square"
                                            )
                                            .resizable()
                                            .foregroundColor(Color("Color2"))
                                            .frame(
                                                width: 16,
                                                height: 16,
                                                alignment: .trailing
                                            )
                                            .padding()
                                        }
                                    }
                                }
                            }
                        }
                        .background(Color(.white))
                        .frame(minWidth: 0,
                               maxWidth: 380,
                               alignment: .center
                        )
                        .cornerRadius(10)
                    }
                }
                HStack {
                    //Move to Navigation bar
                    Button(action: {
                        viewStore.send(.userDidTapCreateElection)
                    }, label: { Text("Create election")
                            .font(.headline)
                            .foregroundColor(Color.white)
                            .frame(maxWidth: .infinity, minHeight: 40)
                            .background(Color.gray)
                        .cornerRadius(15)}
                    )
                }.padding()
            }
            .edgesIgnoringSafeArea(.top)
            .sheet(
                isPresented: viewStore.binding(
                    get: { $0.isElectionTypePresented },
                    send: ElectionAction.setElectionTypePresented
                )
            ) {
                ElectionTypeView(store: store)
            }
        }
    }
}

// MARK: Toggle Button
struct toggleButton: View {
    @Binding var givenStateObject: Bool
    var outputString: String
    
    var body: some View {
        Button(action: {
            self.givenStateObject.toggle()
        }) {
            Text(outputString)
        }
    }
}



struct CreateElectionView_Previews: PreviewProvider {
    static var previews: some View {
        CreateElectionView(
            store: .init(
                initialState: .init(
                    election: .init(
                        name: "",
                        description: "",
                        electionType: "",
                        candidates: [],
                        voterListVisibility: true,
                        startingDate: .init(),
                        endingDate: .init(),
                        isInvite: true,
                        isRealTime: true
                    ),
                    electionTypes: ["type 1", "type 2"]
                ),
                reducer: electionReducer,
                environment: .init()
            )
        )
    }
}
