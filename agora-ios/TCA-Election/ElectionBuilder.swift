//
//  ElectionBuilder.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 27.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation
import SwiftUI
import ComposableArchitecture

class ElectionBuilder {
    var rootViewController: UIViewController?
    let store: Store<ElectionState, ElectionAction>
    let electionFlowController: ElectionFlowController
    
    init() {
        self.electionFlowController = ElectionFlowController()
        let electionEnvironment = ElectionEnvironment()
        self.store = ElectionStore.init(
            initialState: .init(
                election: .init(
                    name: "",
                    description: "",
                    electionType: "",
                    candidates: [],
                    voterListVisibility: true,
                    startingDate: .init(),
                    endingDate: .init(),
                    isInvite: true,
                    isRealTime: true
                ),
                electionTypes: ["type 1", "type 2"]
            ),
            reducer: electionReducer,
            environment: electionEnvironment
        )
    }
    
    func makeCreateElectionView() -> some View {
        CreateElectionView(store: store)
    }
    
    func makeElectionTypeView() -> UIViewController {
        let view = ElectionTypeView(store: store)
        let hosting = UIHostingController(rootView: view)
        return hosting
    }
}
