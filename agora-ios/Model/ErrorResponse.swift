//
//  ErrorResponse.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 14.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation

struct ErrorResponse: Codable, Error {
    let status: String
    let error: String
    
    var errorMessage: String {
        return "\(status): \(error)"
    }
}
