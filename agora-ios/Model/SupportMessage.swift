//
//  SupportMessage.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 26.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation

class SupportMessage {
    static var errorMessage = "Something went very wrong"
    static var defaultMessage = "Please sign up"
}
