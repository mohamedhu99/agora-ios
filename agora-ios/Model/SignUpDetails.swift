//
//  SignUpDetails.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 08.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation

struct SignUpDetails: Codable, Equatable { // Registration User
    static func == (lhs: SignUpDetails, rhs: SignUpDetails) -> Bool {
        lhs.id == rhs.id
    }
    
    struct SecurityQuestion: Codable {
        var crypto: String
        var question: String
        var answer: String
    }
    
    var identifier: String
    var password: String
    var email: String
    var firstName: String
    var lastName: String
    var securityQuestion: SecurityQuestion
    
    var id: String {
        identifier
    }
}

