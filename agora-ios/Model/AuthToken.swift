//
//  AuthToken.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 08.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation

struct AuthToken: Codable, Equatable {
    let token: String
    let expiresOn: String 
}
