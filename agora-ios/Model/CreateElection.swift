//
//  CreateElection.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 22.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation

struct CreateElection: Equatable {
    var name: String
    var description: String
    var electionType: String
    var candidates: [String]
    var ballotVisibility: String?
    var voterListVisibility: Bool
    var startingDate: Date
    var endingDate: Date
    var isInvite: Bool
    var isRealTime: Bool
    var votingAlgo: String = "" // required by API, not present in the UI yet
    var noVacancies: Int?
    var ballot: Ballot?
}

struct Ballot: Equatable {
    var voteBallot: String
    var hash: String
}
