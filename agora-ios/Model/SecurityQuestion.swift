//
//  SecurityQuestion.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 11.08.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation

struct SecurityQuestion: Identifiable {
    let id = UUID()
    let question: String
}

