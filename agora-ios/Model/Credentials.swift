//
//  Credentials.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 19.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation

struct Credentials: Codable, Equatable {
    let identifier: String
    let password: String
    let trustedDevice: String
}
