//
//  User.swift
//  agora-ios
//
//  Created by Aleksandra Komagorkina on 22.07.2022.
//  Copyright © 2022 HalfPolygon. All rights reserved.
//

import Foundation

struct User: Codable, Equatable {
    static func == (lhs: User, rhs: User) -> Bool {
        lhs.id == rhs.id
    }
    
    var id: String { username }
    
    let username: String
    let email: String
    let firstName: String
    let lastName: String
    let avatarURL: String
    var twoFactorAuthentication: Bool = true
    let authToken: AuthToken
    let refreshToken: RefreshToken
}
