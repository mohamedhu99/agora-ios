//
//  agora_iosTests.swift
//  agora-iosTests
//
//  Created by Siddharth sen on 3/17/20.
//  Copyright © 2020 HalfPolygon. All rights reserved.
//

import Combine
import XCTest
import ComposableArchitecture
@testable import agora_ios

class agora_iosTests: XCTestCase {
    private var cancellables: Set<AnyCancellable>!
    
    let user = SignUpDetails(
        identifier: "",
        password: "",
        email: "",
        firstName: "",
        lastName: "",
        securityQuestion: .init(
            crypto: "",
            question: "",
            answer: ""
        )
    )
    
    let sessionLoader = SessionLoader()

    let mainQueue = DispatchQueue.test
    var store: TestStore<AuthState, AuthState, AuthAction, AuthAction, AuthEnvironment>!
    
    override func setUp() {
        cancellables = []
        store = .init(
            initialState: .init(
                signUpDetails: SignUpDetails.init(
                    identifier: UUID().uuidString,
                    password: "12345678",
                    email: "your@mail.com",
                    firstName: "First Name",
                    lastName: "Last Name",
                    securityQuestion: .init(
                        crypto: "crypto",
                        question: "Hello world",
                        answer: "Hello world")
                ), userMessage: SupportMessage.defaultMessage
            ),
            reducer: authReducer,
            environment: .init(
                mainQueue: mainQueue.eraseToAnyScheduler(),
                uuid: UUID.init,
                authBusinessController: SessionLoader(), flowController: AuthFlowController()
            )
        )
    }
    
    func testAuthRequest() {
        var serverMessage = ""
        var errorResponse: ErrorResponse?
        let expectation = self.expectation(description: "Sign Up Request")
        sessionLoader
            .signUp(withUser: user)
            .sink { completion in
                switch completion {
                case .finished:
                    break
                case let .failure(error):
                    errorResponse = error as? ErrorResponse
                }
                expectation.fulfill()
            } receiveValue: { value in
                serverMessage = value
            }
            .store(in: &cancellables)
        waitForExpectations(timeout: 10)
        XCTAssertTrue(serverMessage.isEmpty)
        XCTAssertNotNil(errorResponse?.errorMessage)
    }
    
    // test reducer for correct changing state.userMessage, which means it gets some kind of value
    func testAuthFailed() {
        store.send(AuthAction.AuthFailed) { state in
            state.userMessage = SupportMessage.errorMessage
        }
    }
    
    func testServerResponded() {
        let response = "Server response"
        store.send(.serverResponded(response)) { state in
            state.userMessage = response
        }
    }
    
//    func testAuthPressed() {
//        let message = "Bad request: user already exists"
//        store.send(.AuthPressed)
//        mainQueue.advance(by: 20)
//        store.receive(.serverResponded(message)) { state in
//            state.userMessage = message
//        }
//    }
}

